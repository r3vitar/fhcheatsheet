import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fh-card',
  template:`
  <fh-b-container>
      <ng-content></ng-content>
  </fh-b-container>
  `,
  styleUrls: []
})
export class CardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
