import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fh-b-container',
  template: `<div class="container">
  <div class="row align-items-center">
      
          <ng-content></ng-content>
  </div>
</div>`,
  styleUrls: []
})
export class BContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
