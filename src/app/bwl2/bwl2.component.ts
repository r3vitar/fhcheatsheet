import { AfterContentInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTabGroup } from '@angular/material/tabs';

@Component({
  selector: 'fh-bwl2',
  templateUrl: './bwl2.component.html',
  styleUrls: ['./bwl2.component.scss']
})
export class Bwl2Component implements OnInit {

  constructor() { }
  


  ngOnInit(): void {
  }

  umsatzErloese?: number;
  betriebsErgebnis?: number;

  get umsatzRendite(): number | undefined {
    return this.umsatzErloese && this.betriebsErgebnis 
    ? this.round((this.betriebsErgebnis / this.umsatzErloese) * 100, 2)
    : undefined;
  }

  set umsatzRendite(ur: any) {}

  jahresUeberschuss?: number;
  eigenKapital?: number;
  gesamtKapital?: number;

  get eigenkapitalRendite(): number | undefined {
    return this.jahresUeberschuss && this.eigenKapital 
    ? this.round((this.jahresUeberschuss / this.eigenKapital) * 100, 2)
    : undefined;
  }

  set eigenkapitalRendite(x:any){}

  get eigenkapitalQuote(): number | undefined {
    return this.gesamtKapital && this.eigenKapital 
    ? this.round((this.eigenKapital / this.gesamtKapital) * 100, 2)
    : undefined;
  }

  set eigenkapitalQuote(x:any){}

  liquideMittel?: number;
  kurzFristVerb?: number;
  umlaufVermoegen?: number;

  get lq1(): number | undefined {
    return this.liquideMittel && this.kurzFristVerb 
    ? this.round((this.liquideMittel / this.kurzFristVerb), 2)
    : undefined;
  }

  set lq1(x:any){}

  get lq3(): number | undefined {
    return this.umlaufVermoegen && this.kurzFristVerb 
    ? this.round((this.umlaufVermoegen / this.kurzFristVerb), 2)
    : undefined;
  }

  set lq3(x:any){}

  anlageVermoegen?: number;

  get anlageDeckungsGrad(): number | undefined {
    return this.umlaufVermoegen && this.kurzFristVerb 
    ? this.round((this.umlaufVermoegen / this.kurzFristVerb) *100, 2)
    : undefined;
  }

  set anlageDeckungsGrad(x:any){}
 
 
  private round(x: number, y: number = 0) {
    const ten = Math.pow(10, y);
    return Math.round(x * ten) / ten;
  }
} 
