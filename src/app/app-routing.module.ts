import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { Bwl2Component } from './bwl2/bwl2.component';
import { HomeComponent } from './home/home.component';

export const routes: Array<Route & {linkName?: string, dname?: string}> = [
  {
    path: "",
    pathMatch: "full",
    linkName: "Home", 
    component: HomeComponent,
  }, {
    path: "bwl/2",
    linkName: "BWL 2",
    component: Bwl2Component,
   
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
