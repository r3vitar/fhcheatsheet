import { Component, OnInit } from '@angular/core';
import { routes } from '../app-routing.module';

@Component({
  selector: 'fh-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  links: Array<any> =[];

  constructor () {

    this.links = routes.map(route => ({place: !route.path || route.path === "" ? [""] : route.path.split("/"), name: route.linkName ?? route.dname ?? route.path ?? "wtf"})).slice(1);

  }
  ngOnInit(): void {
  }

}
