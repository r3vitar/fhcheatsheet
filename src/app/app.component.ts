import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {  routes } from './app-routing.module';

@Component({
  selector: 'fh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fh-cheatsheet';

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  links: Array<any> =[];

  constructor (private breakpointObserver: BreakpointObserver) {

    this.links = routes.map(route => ({place: !route.path || route.path === "" ? [""] : route.path.split("/"), name: route.linkName ?? route.dname ?? route.path ?? "wtf"}))

  }
}
